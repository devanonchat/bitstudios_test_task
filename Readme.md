## Реализация

### Требования

Требования для запуска:

- Unix/POSIX-совместимая операционная система (Linux/MacOS)
- ruby
- bash/sh/zsh
- md5sum

Для запуска используйте скрипт ./demonstration.sh

Скрипт выполнит:
- создание тестовых файлов размером 10МБ (10 штук)
- создание дубликатов на основе тестовых файлов
- запуск тестового задания (поиск дубликатов в папке датасета)

Ожидаемый вывод:


```
$ ./demonstration.sh
Generated 10 test files in dataset folder
Now generating duplicates
Done, dataset dir now contains:
data_1
data_10
data_2
data_3
data_4
data_5
data_6
data_7
data_8
data_9
data_dup1_0
data_dup5_0
data_dup5_1
data_dup5_2
data_dup9_0
data_dup9_1
I have found 16 files
Duplicates:
[
  ["dataset/data_dup9_1", "dataset/data_dup9_0", "dataset/data_9"],
  ["dataset/data_1", "dataset/data_dup1_0"],
  ["dataset/data_dup5_0", "dataset/data_dup5_1", "dataset/data_dup5_2", "dataset/data_5"],
]
```

## Оригинальная постановка задания

Есть папка в которой есть много (более 100000) файлов. Контент файлов не важен. Но будем предполагать, что средний размер файла 10 Мб.
Файлы считаются одинаковыми, если их контент побайтово одинаков.

Задача:

Написать скрипт на Ruby, который выведет список файлов, имеющих дубликаты, сгруппированный по содержимому файлов.
Пример:

```
[
        [ DCIM002.jpg, DCIM4432.jpg, DCIM9993_edited.jpeg ],
        [ description.txt, empty.txt, some_name.biz ]
]
```

В оценке задания будет оцениваться правильность результата, сложность(эффективность) алгоритма и читаемость кода.
