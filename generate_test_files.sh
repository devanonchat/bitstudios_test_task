#!/bin/bash

DATASET_DIR='dataset'
NUM=10
FILE_PREFIX='data_'

seq 1 $NUM | xargs -I\# dd if=/dev/urandom bs=1M count=10 of=$DATASET_DIR/$FILE_PREFIX\# 2>/dev/null

echo "Generated $NUM test files in $DATASET_DIR folder"
echo "Now generating duplicates"

cp "$DATASET_DIR/$FILE_PREFIX""1" "$DATASET_DIR/$FILE_PREFIX""dup1_0"

cp "$DATASET_DIR/$FILE_PREFIX""5" "$DATASET_DIR/$FILE_PREFIX""dup5_0"
cp "$DATASET_DIR/$FILE_PREFIX""5" "$DATASET_DIR/$FILE_PREFIX""dup5_1"
cp "$DATASET_DIR/$FILE_PREFIX""5" "$DATASET_DIR/$FILE_PREFIX""dup5_2"

cp "$DATASET_DIR/$FILE_PREFIX""9" "$DATASET_DIR/$FILE_PREFIX""dup9_0"
cp "$DATASET_DIR/$FILE_PREFIX""9" "$DATASET_DIR/$FILE_PREFIX""dup9_1"

echo 'Done, dataset dir now contains:'
ls -1 $DATASET_DIR
