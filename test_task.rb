# frozen_string_literal: true

Dir[File.join(__dir__, 'lib', '*.rb')].each { |file| require file }

df = DuplicateFinder.new("dataset/data_*")
df.perform
