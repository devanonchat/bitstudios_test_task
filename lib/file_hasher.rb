# frozen_string_literal: true

# calculates a checksum of a given file, this implementation depends on md5sum
class FileHasher
  def self.checksum(filepath)
    result = `md5sum #{filepath}`
    raise "Cannot execute md5sum on #{filepath}" if result.empty?

    result.split(' ').first
  end
end
