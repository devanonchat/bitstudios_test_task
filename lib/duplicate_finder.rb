# frozen_string_literal: true

# Main class, provide it with a path to a directory containing dataset
class DuplicateFinder
  def initialize(path)
    @infiles = Dir[path]
    @checksums = {}
  end

  def perform
    puts "I have found #{@infiles.count} files"
    calc_checksums
    present
  end

  private

  def calc_checksums
    @infiles.each do |infile|
      sum = FileHasher.checksum(infile)
      @checksums[sum] ||= []
      @checksums[sum] << infile
    end
  end

  def present
    OutputFormatter.present do
      @checksums.each do |_sum, files|
        next if files.count < 2

        puts "  #{files.inspect},"
      end
    end
  end
end
