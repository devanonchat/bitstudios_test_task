# frozen_string_literal: true

# Decorates an output
class OutputFormatter
  def self.present
    puts 'Duplicates:'
    puts '['
    yield
    puts ']'
  end
end
